import React, { useState } from 'react';
import NavMenu from './components/NavMenu';
import AccountCreate from './components/AccountCreate';

import { Container, Row, Col, Table, Button } from 'react-bootstrap';

export default function App() {
	const [accounts, setAccounts] = useState([
		{
			id: 1,
			username: 'johndoe',
			email: 'johndoe@gmail.com',
			gender: 'Male',
			isSelected: false,
		},
		{
			id: 2,
			username: 'marryjordan',
			email: 'marryjordan@gmail.com',
			gender: 'Female',
			isSelected: false,
		},
		{
			id: 3,
			username: 'nataliwillson',
			email: 'nataliwillson@gmail.com',
			gender: 'Female',
			isSelected: false,
		},
	]);

	const addAccount = (account) => {
		let newAccounts = [...accounts];
		let newAccount = { ...account };

		if (accounts.length) {
			let lastItem = { ...newAccounts[newAccounts.length - 1] };
			newAccount['id'] = lastItem.id + 1;
		} else {
			newAccount['id'] = 1;
		}

		setAccounts([...newAccounts, newAccount]);
	};

	const deleteAccount = () => {
		let newAccounts = [...accounts];
		newAccounts = newAccounts.filter((account) => account.isSelected !== true);
		setAccounts([...newAccounts]);
	};

	const selectItem = (id) => {
		let newAccounts = [...accounts];
		newAccounts.map((account) => {
			if (account.id === id) {
				account.isSelected = !account.isSelected;
			}
			return account;
		});
		setAccounts([...accounts]);
	};

	return (
		<div>
			<NavMenu />
			<Container>
				<Row>
					<Col md={4} className='mt-3'>
						<AccountCreate addAccount={addAccount} />
					</Col>
					<Col md={8} className='my-5'>
						<h3>Table Accounts</h3>
						<Table striped bordered hover>
							<thead>
								<tr>
									<th>#</th>
									<th>Username</th>
									<th>Email</th>
									<th>Gender</th>
								</tr>
							</thead>
							<tbody>
								{accounts.map((account) => (
									<tr
										key={account.id}
										onClick={() => {
											selectItem(account.id);
										}}
										className={account.isSelected ? 'bg-dark text-white' : ''}
									>
										<td>{account.id}</td>
										<td>{account.username}</td>
										<td>{account.email}</td>
										<td>{account.gender}</td>
									</tr>
								))}
							</tbody>
						</Table>
						{accounts.length ? (
							<Button onClick={deleteAccount} variant='danger' type='button'>
								Delete
							</Button>
						) : (
							''
						)}
					</Col>
				</Row>
			</Container>
		</div>
	);
}
