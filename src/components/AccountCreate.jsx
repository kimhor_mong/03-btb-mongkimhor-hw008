import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

export default function AccountCreate({ addAccount }) {
	const schema = yup.object().shape({
		username: yup.string().required(),
		gender: yup.string().required(),
		email: yup.string().email().required(),
		password: yup.string().min(6).required(),
	});

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data, e) => {
		addAccount({ ...data, id: 0 });
		e.target.reset();
	};

	return (
		<Form onSubmit={handleSubmit(onSubmit)}>
			<div className='text-center'>
				<i className='fas fa-user-circle fa-7x'></i>
				<h3>Create Account</h3>
			</div>

			<Form.Group>
				<Form.Label>Username</Form.Label>
				<Form.Control {...register('username')} type='text' placeholder='Username' />
				<p className='text-danger'>{errors.username?.message}</p>
			</Form.Group>

			<Form.Group>
				<Form.Label className='d-block'>Gender</Form.Label>
				<Form.Check {...register('gender')} inline type='radio' label='Male' value='Male' checked />
				<Form.Check {...register('gender')} inline type='radio' label='Female' value='Female' />
			</Form.Group>

			<Form.Group>
				<Form.Label>Email</Form.Label>
				<Form.Control {...register('email')} type='text' placeholder='Email' />
				<p className='text-danger'>{errors.email?.message}</p>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control {...register('password')} type='password' placeholder='Password' />
				<p className='text-danger'>{errors.password?.message}</p>
			</Form.Group>

			<Button variant='primary' type='submit'>
				Save
			</Button>
		</Form>
	);
}
