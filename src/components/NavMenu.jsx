import React from 'react';
import { Container, Navbar } from 'react-bootstrap';

export default function NavMenu() {
	return (
		<Navbar>
			<Container>
				<Navbar.Brand href='#home'>KSHRD Student</Navbar.Brand>
				<Navbar.Toggle />
				<Navbar.Collapse className='justify-content-end'>
					<Navbar.Text>
						Signed in as: <a href='#login'>Mark Otto</a>
					</Navbar.Text>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}
